import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class Main {

    
    // you only need to change the main function here.
	public static void main(String[] args) throws Exception {
        Class.forName("org.h2.Driver");
        
        String DBurl = "jdbc:h2:tcp://localhost:9092/~/test";
        
        if (args != null && args.length > 0) DBurl = args[0];
        
        Connection conn = DriverManager.getConnection(DBurl, "sa", "");
           
        DatabaseMetaData dbmd = conn.getMetaData();
    	System.out.println("Database system: " 	+ dbmd.getDatabaseProductName());
    	System.out.println("Version        : "	+ dbmd.getDatabaseMajorVersion() + "." + dbmd.getDatabaseMinorVersion());
   
    	
        // lets have a look on all tables
    	ResultSet rs = dbmd.getTables(null, null, "%", null);
    	while (rs.next()) {
            System.out.println(rs.getString(3));
    	}
    	rs.close();
    	
    	// do something else ...
        
        conn.close();
	}
	
}


